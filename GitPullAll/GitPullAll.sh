#/bin/bash

USER=`whoami`
DIRS=`cat repodir.conf`

for j in ${DIRS}
do
    cd /home/${USER}/${j}
    for i in `ls`
    do
        cd ${i}
        echo "Checking for updates in "`git config --get remote.origin.url`
        git pull --rebase=false &
        cd ..
    done
done


