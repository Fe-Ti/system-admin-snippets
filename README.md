# System Admin Snippets

## ORC
#### What
Script for using OpenRC as a simpler (hence better) version of systemd (**Note**: only its 'init' part).
#### Why
Because on Devuan there are `rc-update <option> <your service>` and `rc-service <your service> <option>` without autocompletion. I usually forget the order of arguments so here we have an easy-to-use command `orc` which has autocompletion for bash. 
One script to rule them all.
#### HOWTO
Install via running install.sh in orc directory with root privileges.
JThen just in this way:
```
root@<your_PS1>$ orc <add|del|start|stop|restart|reload|list|enable|disable> <service_name> [run_level]
```
**Note**: run_level is meaningful only for `add` and `del`.

## gen-ssh-keys
#### What
Simple SSH&GPG key generator script
#### Why
Because the appropriate commands for generating keys are easy to forget.
#### HOWTO
Edit the EMAIL variable in the script and just run it in you terminal emulator. 

## GitPullAll
#### What
Autopull changes.
#### Why
Because I have a lot of repositories and I'm very lazy admin.
#### HOWTO
Edit `repodir.conf` placing a path to each directory with cloned repos, e.g. `GitlabRepos` means `/home/<username>/GitlabRepos`. The script will go through each repo in the directory pulling changes from origin, so this can take a while.

## init_new_home_repo
#### What
Simple script which creates an empty repo on the server which you own.
#### Why
Just because :) .
#### HOWTO
Just run it.

## clang-format
#### What
Style file for clang-format utility.
#### Why
I love beautiful code. And I'm lazy. So.
#### HOWTO
Rename and place to wanted dir.

