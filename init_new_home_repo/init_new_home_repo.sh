#!/bin/bash

server="192.168.1.7" # here your home server IP goes
username=`git config --get user.user`
git_cmds='whoami; echo "Enter repository name:" && read NAME && git init --bare ${username}/$NAME'

ssh git@${server} ${git_cmds}

exit

