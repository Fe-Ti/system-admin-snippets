EMAIL='<YOUR EMAIL>'

cd ~

echo 'Do you want to make SSH key? (yes/no)'
read MAKE_SSH_KEY
if [ ${MAKE_SSH_KEY} = "yes" ]
then
ssh-keygen -t ed25519 -C ${EMAIL}
ssh-add
echo 'Your public key (copy and paste where you need)':
cat .ssh/id_ed25519.pub
fi

echo 'Do you want to make GPG key? (yes/no)'
read MAKE_GPG_KEY
if [ ${MAKE_GPG_KEY} = "yes" ]
then
gpg --full-gen-key
KEY_ID=`gpg --list-secret-keys --keyid-format LONG ${EMAIL} | grep sec | awk -F /  '{ print $2 }' | awk -F ' ' '{ print $1 }' | tail -n 1`
gpg --armor --export ${KEY_ID}
echo '\n\nConfiguring git for using the key...'
git config --global user.signingkey ${KEY_ID}
fi

echo 'Thanks for using this script!'
exit

